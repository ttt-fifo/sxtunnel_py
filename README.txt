sxtunnel - Socks tunnel
=======================

Author: Todor Todorov <ttodorov@null.net>
License: BSD 2-clause "Simplified" License

Helps socks unaware applications to connect via socks proxy.

Typical usage:

         local address   socks address                       server address
         (conf:accept)   (conf:uplink)                       (conf:connect)
            |              |                                    |
            |              |                  ***********       |
+-----+     | +------+     | +-------+     *** Internet  ***    | +--------+
| app +------>| tunn +------>| socks |...**.................**...>| server |
+-----+       +------+       +-------+     ***           ***      +--------+
                                              ***********
socks         sxtunnel       socks                                destination
unaware                      proxy                                server
application


NOTE: the connection between app and server can be plaintext, tls/ssl or 
      encrypted any other way. The sxtunnel and socks transport the network
      packets between the app and the server without changing them.
NOTE: no multiclient, designed only for one user (may be redesigned if needed)
TODO: can be extended to more socks versions (currently support v5), can be
      extended to http proxying, possible to be multiclient.

Alternatives:
I think socat is SOCKS aware, but never have tested it. I would appreciate
if someone has more ideas for alternatives to contact me back with this info.
