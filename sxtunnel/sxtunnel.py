#!/bin/env python3
"""
sxtunnel - Socks tunnel.

Author: Todor Todorov <ttodorov@null.net>
License: BSD 2-clause "Simplified" License

Helps socks unaware applications to connect via socks proxy
"""
import sys
import socket
import select
import time
from datetime import datetime
import socks

# length of the buffer for all network operations
BUFFER_LEN = 1024

# log levels
CRIT = 0
WARN = 1
INFO = 2
DEBG = 3


class Log:
    """Create logging object."""

    # human readable loglevels
    names = {
        CRIT: "CRIT",
        WARN: "WARN",
        INFO: "INFO",
        DEBG: "DEBG",
        }

    def __init__(self, loglevel):
        """
        Initialize object.

        loglevel: the current loglevel

        object data:
        self.loglevel: the current loglevel for all logging operations
        """
        self.loglevel = loglevel

    def __call__(self, loglevel, string):
        """Log a string."""
        if self.loglevel < loglevel:
            # do not log this string, because it is with higher loglevel
            return

        # output to stdout (and flush)
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f"{date} [{self.names[loglevel]}] {string}",
              flush=True)


class Conf:
    """Create object for parsing configuration file."""

    def __init__(self, filename):
        """
        Initialize object.

        filename: the configuration file name

        object data:
        self.filename: configuration file name
        """
        self.conffile = filename

    def parse_hostname_port(self, hostname_port):
        """
        Parse hostname:port into tuple.

        hostname_port: string of kind "127.0.0.1:2020"
        Returns: tuple of kind ('127.0.0.1', 2020)
        """
        hostname, port = hostname_port.split(':', 1)
        port = int(port)
        return (hostname, port)

    def parse_conffile(self):
        """
        Parse configuration file.

        Returns: dict with configurations
        """
        conf = {}
        with open(self.conffile, "r") as confd:
            for line in confd.readlines():
                # no blanks, no comments
                line = line.strip()
                if not line:
                    continue
                if line.startswith('#'):
                    continue

                key, val = line.split("=", 1)
                key = key.strip()
                val = val.strip()

                conf[key] = val

        conf['loglevel'] = int(conf['loglevel'])

        # ip, port tuples
        for key in ['accept', 'connect', 'uplink']:
            conf[key] = self.parse_hostname_port(conf[key])

        return conf


class LocalConnection:
    """Create local connection object."""

    def __init__(self, conf, logging):
        """
        Initialize object.

        conf: configuration dict
        logging: logging object

        object data:
        self.conf: configuration dict
        self.logging: logging object
        self.insock: listening input socket
        self.clisock: the socket of the connected client
        self.cliinfo: the (ip, port) client information tuple
        """
        self.conf = conf
        self.logging = logging
        self.insock = None
        self.clisock = None
        self.cliinfo = None

    def listen(self):
        """Open listening socket for client connections."""
        self.logging(INFO, "Listening" +
                     f" {self.conf['accept'][0]}" +
                     f":{self.conf['accept'][1]}")
        try:
            self.insock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.insock.bind(self.conf['accept'])
            self.insock.listen(1)
        except Exception as e:
            self.logging(WARN, str(e))

    def listen_end(self):
        """Close the listening socketi."""
        self.logging(INFO, "Listen end")
        try:
            self.insock.shutdown(1)
            time.sleep(0.2)
            self.insock.close()
        except Exception as e:
            self.logging(WARN, str(e))
        self.insock = None

    def wait_client(self):
        """Wait for client connection on listening socket."""
        self.logging(INFO, "Waiting for client connection")
        try:
            self.clisock, self.cliinfo = self.insock.accept()
            self.logging(INFO, "Client connected from" +
                         f" {self.cliinfo[0]}:{self.cliinfo[1]}")
        except Exception as e:
            self.logging(WARN, str(e))

    def disconnect_client(self):
        """Disconnect the connected client."""
        self.logging(INFO, "Disconnecting client")
        try:
            self.clisock.close()
        except Exception as e:
            self.logging(WARN, str(e))
        self.clisock = None
        self.cliinfo = None

    def recv(self):
        """
        Receive data from client socket.

        Returns: the data received
        """
        try:
            data = self.clisock.recv(BUFFER_LEN)
        except Exception as e:
            self.logging(WARN, str(e))
            return None
        return data

    def sendall(self, data):
        """
        Send data to client socket.

        Returns: True=success, False=error
        """
        try:
            self.clisock.sendall(data)
        except Exception as e:
            self.logging(WARN, str(e))
            return False
        return True


class RemoteConnection:
    """Create object for remote server connection."""

    def __init__(self, conf, logging):
        """
        Initialize object.

        conf: configuration dict
        logging: logging object

        object data:
        self.conf: configuration dict
        self.logging: object for logging
        self.outsock: socket for outbound connection to the remote server
        """
        self.conf = conf
        self.logging = logging
        self.outsock = None

    def connect(self):
        """Connect to remote server through socks proxy."""
        self.outsock = socks.socksocket()
        self.logging(INFO, "Connecting " +
                     f"->{self.conf['uplink'][0]}:{self.conf['uplink'][1]}" +
                     f"->{self.conf['connect'][0]}:{self.conf['connect'][1]}")
        try:
            self.outsock.set_proxy(socks.SOCKS5,
                                   self.conf['uplink'][0],
                                   self.conf['uplink'][1])
        except Exception as e:
            self.logging(WARN, f"Uplink: {str(e)}")
        try:
            self.outsock.connect(self.conf['connect'])
            self.logging(INFO, "Connected")
        except Exception as e:
            self.logging(WARN, f"Endpoint: {str(e)}")

    def disconnect(self):
        """Disconnect the remote server."""
        self.logging(INFO, "Disconnecting remote")
        try:
            self.outsock.shutdown(1)
            time.sleep(0.2)
            self.outsock.close()
        except Exception as e:
            self.logging(WARN, str(e))
        self.outsock = None

    def sendall(self, data):
        """
        Send data to remote socket.

        Returns: True=success, False=error
        """
        try:
            self.outsock.sendall(data)
        except Exception as e:
            self.logging(WARN, str(e))
            return False
        return True

    def recv(self):
        """
        Receive data from remote socket.

        Returns: data received
        """
        try:
            data = self.outsock.recv(BUFFER_LEN)
        except Exception as e:
            self.logging(WARN, str(e))
            return None
        return data


class Proxy:
    """Create object for proxying between client and remote server."""

    def __init__(self, local_conn, remote_conn, logging):
        """
        Initialize object.

        local_conn: local connection object
        remote_conn: remote server connection object
        logging: logging object

        object data:
        self.local_conn: local connection object
        self.remote_conn: remote server connection object
        self.logging: logging object
        """
        self.local_conn = local_conn
        self.remote_conn = remote_conn
        self.logging = logging

    def tunnel(self):
        """Tunnel between client and remote server via uplink socks proxy."""
        self.logging(INFO, "Tunneling between client and server")

        # construct sockets list
        try:
            sock_list = [self.local_conn.clisock, self.remote_conn.outsock]
        except Exception as e:
            self.logging(WARN, str(e))
            return

        while True:
            # select on sockets
            try:
                # this blocks until there are ready sockets
                sockets_ready, _, _ = select.select(sock_list, [], [])
            except Exception as e:
                self.logging(WARN, str(e))
                return

            # process each socket with data
            for sock in sockets_ready:
                if sock == self.local_conn.clisock:
                    # client socket has data for processing
                    data = self.local_conn.recv()
                    if not data:
                        self.logging(WARN, "Client disconnect")
                        return
                    result = self.remote_conn.sendall(data)
                    if not result:
                        self.logging(WARN, "Server disconnect")
                        return
                elif sock == self.remote_conn.outsock:
                    # remote server socket has data for processing
                    data = self.remote_conn.recv()
                    if not data:
                        self.logging(WARN, "Server disconnect")
                        return
                    result = self.local_conn.sendall(data)
                    if not result:
                        self.logging(WARN, "Client disconnect")
                        return

    def __call__(self):
        """
        Execute proxy cadance.

        * open listening socket for client connection locally
        * wait for client
        * connect remote server (via socks proxy)
        * tunnel between client <---> remote server
        * disconnect remote
        * disconnect local client
        * end listening on local socket
        * loop indefinitely
        """
        while True:
            self.local_conn.listen()
            self.local_conn.wait_client()
            self.remote_conn.connect()
            self.tunnel()
            self.remote_conn.disconnect()
            self.local_conn.disconnect_client()
            self.local_conn.listen_end()
            time.sleep(2)


def main(conffile):
    """Execute main computing."""
    conf = Conf(conffile).parse_conffile()
    logging = Log(conf['loglevel'])
    local_conn = LocalConnection(conf, logging)
    remote_conn = RemoteConnection(conf, logging)
    proxy = Proxy(local_conn, remote_conn, logging)
    proxy()


if __name__ == "__main__":
    try:
        conf_file = sys.argv[1]
    except IndexError:
        print("Usage:")
        print("sxtunnel.py /path/to/file.conf")
        sys.exit(1)
    main(conf_file)
